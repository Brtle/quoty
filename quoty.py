#! /usr/bin/env python3

import logging
import os
import random
import sys

from discord import Embed
from discord.ext import commands
import wikiquote


async def send_quote(ctx, quote, author):
    color = random.randint(0, 2 ** 24)

    quote_title = quote[:256]
    quote_description = quote[256:]

    embed = Embed(title=quote_title, description=quote_description, color=color)
    embed.set_footer(text=author)

    await ctx.send(embed=embed)


def run_bot(token):
    description = '''A bot to quote some smart ass.'''
    bot = commands.Bot(command_prefix='/', description=description)

    @bot.event
    async def on_ready():
        logging.info(f"Logged in as {bot.user.name} ({bot.user.id})")

    @bot.command()
    async def quote(ctx, *args):
        if args[0] in wikiquote.supported_languages():
            lang = args[0]
            raw_title = ' '.join(args[1:])
        else:
            lang = 'en'
            raw_title = ' '.join(args)

        logging.info(f"Quote requested for '{raw_title}'")

        try:
            title = wikiquote.search(raw_title, lang=lang)[0]

            quotes = wikiquote.quotes(title, lang=lang)

            # Prioritize short quotes
            short_quotes = [q for q in quotes if len(q) <= 256]

            if short_quotes:
                quote = random.choice(short_quotes)
            else:
                quote = random.choice(quotes)
        except IndexError:
            logging.info(f"No quote found")

            await send_quote(ctx, "Quote not found", "404")
        else:
            logging.info(f"Quote found for {title}")
            logging.info(quote)
            await send_quote(ctx, quote, title)

    @bot.command()
    async def qotd(ctx, *args):
        quote, author = wikiquote.quote_of_the_day(lang='fr')
        logging.info(f"Quote of the day, by {author}")
        logging.info(quote)
        await send_quote(ctx, quote, author)

    bot.run(token)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    token = os.environ.get('DISCORD_TOKEN', None)

    if not token:
        logging.error('No DISCORD_TOKEN environment variable found!')
        sys.exit(1)

    run_bot(token)
